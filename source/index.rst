Welcome to SEIC-Systems Mainframe's documentation!
==================================================

What is the Mainframe?
----------------------

The Mainframe is a self contained voice control framework designed for full modularity 
and an online capability that require you to opt in to use it.

Features
--------

Internal database for user and plugin data
__________________________________________
Contains informations about specific users and plugins and allows them to save data themself

Speaker recognition
___________________
Users may be recognized by their voice and have rights according to their userlevel and tags

Integrated MQTT system
______________________
Allows to communicate with connected devices

API for apps, webinterfaces, etc..
__________________________________
Allows to acces userdata, display plugin generated information or call plugin functions

________________________________________________

.. toctree::
   :maxdepth: 2
   :caption: Learn more:
   
   usage/quickstart
   usage/modules
   concepts/audio-io
