Modules and you
===============

Modules
-------

The .meta file
~~~~~~~~~~~~~~

Contains required ``external packages`` for installation with ``apt`` and ``pipenv``::

   {
     packages: ['package1', 'package2'],
     pypackages: ['py-package1', 'py-package2']
   }

Plugin
~~~~~~

Contains the functional part of the module, that means it validates the users and checks their permission, 
loads and saves data from and to the database and executes the actual code.

Can import other plugins to use in its own code.

Command
~~~~~~~

Uses the extracted audio data to determine if it needs to execute some functionality of the plugins it imports.
