Get started
===========

Setup environment
-----------------

Clone repository
~~~~~~~~~~~~~~~~

Clone from gitlab::

   $ git clone --recursive https://gitlab.com/SEIC-Systems/HomeAuto

Install packages
~~~~~~~~~~~~~~~~

Install base packages::

   $ sudo apt install -y python3 python3-pip ffmpeg portaudio19-dev

Install mongodb
_______________

Add repository key::

   $ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4

Add the repository::

   $ echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/4.0 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list

Update package database and install ``mongodb``::

   $ sudo apt update && sudo apt install -y mongodb-org

Setup mongodb
_____________

(Optional) Install pyenv
________________________

.. note::
   
   You only need to do this if you don't have the required python version installed.
   
Install required packages::

   $ apt install -y #fillme

Install ``pyenv`` using its installer::

   $ curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash

Setup pipenv
~~~~~~~~~~~~ 

Install ``pipenv`` using pip3::

   $ pip3 install --user --upgrade pipenv

Install packages from the ``Pipfile``::

   $ cd HomeAuto
   $ pipenv install

.. note::

   While most required packages should have been installed during the previous steps,
   please ensure that all required dev packages are installed, as they may vary between systems.

Configure system
----------------

The global configurations lives in ``karen.conf``. 
There should be no need to change any settings for the system to work.

.. todo::

   Create automatic audio configurator to be run on the first start.
   -> Sets basic microphone settings and configures filters
   
Setup user account
~~~~~~~~~~~~~~~~~~

Login as ``admin`` with the password ``admin``.
Change the password and create a private account.

.. note::
   
   If you set the ``MISC/admin_override`` in the config to ``True`` the current admin password will be overwritten with its default value.
   To circumvent a security breach by forgetting to unset the override, it will be reverted after the first successful login. 

Train piwho model
~~~~~~~~~~~~~~~~~

(Optional) Activate sentry.io
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can find the sentry settings in the ``LOGGING`` section of the config file.

To use sentry set the ``LOGGING/sentry`` option to ``True`` and paste your sentry dns into ``LOGGING/sentry_dns`` 