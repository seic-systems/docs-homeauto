Audio IO system
===============

Room
----

Group of one microphone and one or more speakers that have their own session of the commandmaster to take in audio data, extracts users and intentions, run appropriate commands and return (audio-)responses separated from other rooms.

Audio layer system
------------------

This system generates audio output by combining audio tracks depending on their given settings.

Audio track
-----------

Consecutive audio stream to be played by the audio system.
Must define a priority by which the track is sorted into an appropriate layer.
May pause/delete itself if moved into a given layer and/or remaines there for some time.
Also tracks may set tags, there are some ideas for them but nothing concrete.

Layers
------

Active Layer - One track
________________________
Track plays normal

.. note::
   
   TBD layer size one or more

Background Layer - Variable size
________________________________
Track will be toned down

.. note::

   TBD: Per track tone down percentage

Inactive Layer - Infinite or finite with a variable size
________________________________________________________
Tracks will be muted
